import {NgModule} from '@angular/core';

import {UsersComponent} from "users.component";

import { RouterModule, Routes} from '@angular/router';

export const appRoutes: Routes = [
    {
        path: '/users/:id',
        component: UsersComponent,
    }
];


@NgModule({
	declarations: [UsersComponent],
	imports: [],
	providers: [],
	exports: [UsersComponent]
})

export class UsersModule{}