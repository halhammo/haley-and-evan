import { Component, OnInit } from '@angular/core';
import {ActivateRoute} from '@angular/router';
import {UserService} from "../../components/services/user.service";

@Component({
	selector: 'users',
	template: require('./users.html'),
	styles: [require('./users.scss')],
})

export class UsersComponent implements OnInit{


	constructor(private user: UsersComponent, private activate: ActivateRoute, private userService: UserService){
    this.userService = userService;
	}

	ngOnInit(){

		this.route.params(params => {
      userService.getUserById(params)
        .then(function (userInfo) {
          return userInfo;
        })
		});

	}

}

// export AcitvateRoute{}
