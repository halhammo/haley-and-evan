'use strict';

//allows for unique id generation for users
import uuidv4 from 'uuid/v4';
import {User} from './users.model';

export function index(req, res)
{
  User.find()
  .exec()
  .then(function(users)
  {
    //if user exists, return user
    res.json({
      users
    });
  })

  //if user does not exist
  .catch(function(err){
    res.status(500);
    res.send(err);
  });
}

export function show(req, res)
{
  User.findById(req.params.id)
      .exec()
      //status codes for success/failure
      .then(function(existingUser)
      {
        //if user exists, return user
        if(existingUser)
        {
          res.status(200);
          res.json(existingUser);
        }

        else
        {
          res.status(404);
          res.json({message: 'Not Found'});
        }
      })
      .catch(function(err) {
        res.status(400);
        res.send(err);
      });
}

export function create(req, res) {
  //generate random id for user
  let id = uuidv4();

  let user = req.body;

  // Validate parameter exists and is a string
  if(!user.fullName.firstName || typeof user.fullName.firstName !== 'string') {
    res.status(400);
    return res.json({
      error: 'firstName(String) is required'
    });
  }

  // Validate parameter exists and is a string
  if(!user.fullName.lastName || typeof user.fullName.lastName !== 'string') {
    res.status(400);
    return res.json({
      error: 'lastName(String) is required'
    });
  }

  let userName = user.userName;
  // Validate parameter exists and is a string
  if(!userName || typeof userName !== 'string') {
    res.status(400);
    return res.json({
      error: 'userName(String) is required'
    });
  }

  let email = user.email;
  // Validate parameter exists and is a number
  if(!email || typeof email !== 'string') {
    res.status(400);
    return res.json({
      error: 'email(String) is required'
    });
  }

  User.create(user)
  .then(function(createdUser){
    res.status(201);
    res.json(createdUser);
  })
  .catch(function(err){
    res.status(400);
    res.send(err);
  });
}

export function update(req, res) {

  User.findById(req.params.id)
  .exec()
  .then(function(existingUser){
    if(existingUser){
      existingUser.fullName.firstName = req.body.fullName.firstName;
      existingUser.fullName.lastName = req.body.fullName.lastName;
      existingUser.userName = req.body.userName;
      existingUser.email = req.body.email;

      return Promise.all([
        existingUser.increment().save()
        ]);
    }
    else{
      return existingUser;
    }
  })
   .then(function(savedObjects) {
      if(savedObjects) {
        res.status(200);
        res.json(savedObjects[1]);
      }

      else {
        // User was not found
        res.status(404);
        res.json({message: 'Not Found'});
      }
    })
    // Error encountered during the save of the user or address
    .catch(function(err) {
      res.status(400);
      res.send(err);
    });
}

export function destroy(req, res) {
  User.findById(req.params.id)
    .exec()
    .then(function(existingUser) {
      if(existingUser) {
        return Promise.all([
          existingUser.remove()
        ]);
      } 
      else {
        return existingUser;
      }
    })
    // Delete was successful
    .then(function(deletedUser) {
      if(deletedUser) {
        res.status(204).send();
      } 
      else {
        // User was not found
        res.status(404);
        res.json({message: 'Not Found'});
      }
    })
    // Address or user delete failed
    .catch(function(err) {
      res.status(400);
      res.send(err);
    });
}
