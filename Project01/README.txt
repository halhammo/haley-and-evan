Recipes.model.js

name: String and required because all recipes should have a name
description: String and required because all recipes should have a description
pictureURL: Just a string and not required because you don't NEED a picture
prepTime: Number because it's a unit of time and not required because some people can do things like cut a carrot faster than others
cookTime: Number because it's a unit of time and is required because you need to know how long to cook it
directions: string



.model
   -schema

.controller
   -functions for:
   -validation
   -manipulating the database
   -returning response codes

.index
  -handles CRUD requests received from router
  -call functions in the controller as specified by the request


1. user makes request to router 
2. router routes request to apropraite controller
3. Controller calls function which changes appropraite model
3a. model affects other models if necessary
4. controller verifies that the request happened/didn't happen and sends response back to router
5. router shows status message to user


Routing Paths Made:

request --> routes.js --> _blank.index --> _blank.controller --> MongoDB ~~data manipulation

data manipulation --> seen by _blank.controller --> sends response to _blank.index --> returns to routes.js --> returned to user (requester)



full path

routes goes to

recipe.index or reviews.index or users.index which goes to

recipe.controller or reviews.controller or users.controller which goes to 

recipe.model or reviews.model or users.model which creates models in MongoDB





web-programming-ii/server
-index.js
-routes.js

web-programming-ii/server/api
-recipes (folder)
-reviews (folder)
-users   (folder)

web-programming-ii/server/api/users
-users.index
-users.controller
-users.model

web-programming-ii/server/api/recipes
-recipes.index
-recipes.controller
-recipes.model

web-programming-ii/server/api/reviews
-reviews.index
-reviews.controller
-recipes.model
          