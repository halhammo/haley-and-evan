import mongoose from 'mongoose';
let Schema = mongoose.Schema;

var userSchema = new Schema({
	//subdocument w/ required attributes
	fullName:{
		firstName: {type: String, required: true},
		lastName: {type: String, required: true}
	},
	//field is required and must be unique
	userName: {type: String, unique: true, required: true},
	//unique but not required
	email: {type: String, unique: true}
});

//compiles models from schema
let User = mongoose.model('User', userSchema);

export{User};