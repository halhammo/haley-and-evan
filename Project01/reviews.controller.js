'use strict';

import {Reviews} from './reviews.model';

export function index(req, res)
{
	Reviews.find()
		.populate('User')
		.exec()
		.then(function(reviews)
		{
			//if review exists, return review
			res.json({
				reviews
		});
  })

  //if review does not exist
  .catch(function(err){
    res.status(500);
    res.send(err);
  });
}

export function show(req, res)
{
	Reviews.findById(req.params.id)
		.populate('User')
		.exec()
		.then(function(existingReview){
			//if review exists
			if(existingReview)
			{
				res.status(200);
				res.json(existingReview);
			}
			else
			{
				res.status(404);
				res.json({message: 'Not Found'});
			}
		})
		.catch(function(err){
			res.status(400);
			res.send(err);
		});
}

export function create(req, res)
{
	console.log("recipe create REACHED");

	let reviews = req.body;
	let user = req.body.user;

	// let rating = req.body.rating;
	// // Validate parameter exists and is a string
	// if(!rating || typeof rating !== 'number') {
	//   res.status(400);
	//   return res.json({
	//     error: 'address(number) is required'
	//   });
	// }

 //  	//i think that date is special like array and this is what
	// //i found? I think this is right? 
	// //https://webbjocke.com/javascript-check-data-types/
	// let date = req.body.date;
	//  if(!date || !(date instanceof Date)) {
	//   res.status(400);
	//   return res.json({
	//     error: 'address(number) is required'
	//   });
	// }

	// // let user = req.body.user;
	//  if(!user) {
	// 	res.status(400);
	// 	return res.json({
	// 		error: 'user is required'
	// 	});
	//    }

	Reviews.create(reviews)
	.then(function(createdReviews){
		res.status(201);
		res.json(createdReviews);
	})
	.catch(function(err){
		res.status(400);
		res.send(err);
	});

	// //creates a user for the review
	// User.create(user)
	// 	.then(function(createdUser){
	// 		reviews.user = createdUser;
	// 		return Reviews.create(reviews);
	// 	})

	// 	//status codes for success/failure
	// 	.then(function(createdReviews){
	// 		res.status(201);
	// 		res.json(createdReviews);
	// 	})

	// 	.catch(function(err){
	// 		res.status(400);
	// 		res.send(err);
	// 	});
 }

export function update(req, res)
{
	Reviews.findById(req.params.id)
		.populate('User')
		.exec()

		.then(function(existingReview){
			if(existingReview)
			{
				existingReview.description = req.body.description;
				existingReview.rating = req.body.rating;
				existingReview.date = req.body.date;

				//update the review's user's information
				existingReview.user.fullName.firstName = req.body.user.fullName.firstName;
				existingReview.user.fullName.lastName = req.body.user.fullName.lastName;
				existingReview.user.userName = req.body.user.userName;
				existingReview.user.email = req.body.user.email;

				return Promise.all([
					existingReview.user.increment().save(),
					existingReview.increment().save()
				]);
			}
			//review was not found
			else
			{
				return existingReview;
			}
		})

		//status codes for success/failure
		.then(function(savedObjects){
			if(savedObjects)
			{
				res.status(200);
				res.json(savedObjects[1]);
			}
			else
			{
				res.status(404);
				res.json({message: 'Not Found'});
			}
		})

		//error encountered while saving review/user
		.catch(function(err){
			res.status(400);
			res.send(err);
		});
}

export function destroy(req, res){
	Reviews.findById(req.params.id)
	.populate('user')
	.exec()

	.then(function(existingReview){
		if(existingReview)
		{
			return Promise.all
			([
				existingReview.user.remove(),
				existingReview.remove()
			]);
		}
		else
		{
			return existingReview;
		}
	})

	//status codes for success/failure
	.then(function(deletedReview){
		if(deletedReview)
		{
			res.status(204).send();
		}
		else
		{
			res.status(404);
			res.json({message: 'Not Found'});
		}
	})

	.catch(function(err){
		res.status(400);
		res.send(err);
	});
}
