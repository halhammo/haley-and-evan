'use strict';

import {Recipe} from './recipes.model';

//finds all recipes
export function index(req, res)
{
	Recipe.find()
        .populate('ingredient')
        .exec()
        .then(function(recipes) {
          res.json( {
            recipes
          });
        })
        .catch(function(err){
          res.status(500);
          res.send(err);
        });
}

export function show(req, res) {
  Recipe.findById(req.params.id)
      .populate('ingredient')
      .exec()
      .then(function(existingRecipe) {
        if(existingRecipe) {
          res.status(200);
          res.json(existingRecipe);
        } else {
          res.status(404);
          res.json({message: 'show recipe Not Found'});
        }
      })
      .catch(function(err) {
        res.status(400);
        res.send(err);
      });
}

//creates recipe and validates schema requirements
export function create(req, res) {

    console.log("recipe create REACHED");

  let recipe = req.body;

  // //Validation and binding of schema attributes
  // let name = req.body.name;
  // // Validate parameter exists and is a string
  // if(!name || typeof name !== 'string') {
  //   res.status(400);
  //   return res.json({
  //     error: 'name(String) is required'
  //   });
  // }

  // let description = req.body.description;
  // // Validate parameter exists and is a string
  // if(!description || typeof description !== 'string') {
  //   res.status(400);
  //   return res.json({
  //     error: 'description(String) is required'
  //   });
  // }

  // //not required; no validation
  // let pictureURL = req.body.pictureURL;

  // //not required; no validation
  // let prepTime = req.body.prepTime;

  // let cookTime = req.body.cookTime;
  // // Validate parameter exists and is a number
  // if(!cookTime || typeof cookTime !== 'number') {
  //   res.status(400);
  //   return res.json({
  //     error: 'cookTime(Number) is required'
  //   });
  // }

  // let directions = req.body.directions;
  // // Validate parameter exists and is an array
  // if(!description || !(Array.isArray(description))) {
  //   res.status(400);
  //   return res.json({
  //     error: 'description(array) is required'
  //   });
  // }

  // let ingredients = req.body.ingredients;
  // // Validate parameter exists and is an array
  // if(!ingredients || !(Array.isArray(ingredients))) {
  //   res.status(400);
  //   return res.json({
  //     error: 'ingredients(array) is required'
  //   });
  // }

  // //not required; no validation
  // let userReview = req.body.userReview;


  // Create a new user object with the generated ID and the fields provided by the user
  Recipe.create(recipe)
      .then(function (createdRecipe) {
        // Set a status code of 201 (created) and return the new user object back to the caller
        // You need to return the new user so that they can see the generated ID
        res.status(201);
        res.json(createdRecipe);
      })
      .catch(function (err) {
        res.status(400);
        res.send(err);
      });
}

export function update(req, res)
{
  Recipe.findById(req.params.id)
      .populate('review')
      .exec()
      .then(function (existingRecipe) {
        if(existingRecipe) {
          existingRecipe.name = req.body.name;
          existingRecipe.description = req.body.description;
          existingRecipe.pictureURL = req.body.pictureURL;
          existingRecipe.prepTime = req.body.prepTime;
          existingRecipe.cookTime = req.body.cookTime;
          existingRecipe.directions = req.body.directions;
          existingRecipe.ingredients = req.body.ingredients;
          existingRecipe.userReview = req.body.userReview;

          return Promise.all([
              existingRecipe.increment().save()
          ]);
        } else {
          return existingRecipe;
        }

      })
      .then(function (savedObjects) {
        if(savedObjects) {
          res.status(200);
          res.json(savedObjects[1]);
        } else {
          res.status(404);
          res.json({message: 'Not Found'});
        }
      })
      .catch(function (err) {
        res.status(400);
        res.send(err);

      });
}

export function destroy(req, res){

  Recipe.findById(req.params.id)
      .exec()
      .then(function (existingRecipe) {
        if(existingRecipe) {
          return Promise.all([
              existingRecipe.remove()
          ]);
        }
        else {
          return existingRecipe;
        }
      })
      .then(function (deletedRecipe) {
        if(deletedRecipe) {
          res.status(204).send();
        }
        else {
          res.status(404);
          res.json({message: 'Not Found'});
        }
      })
      .catch(function (err) {
        res.status(400);
        res.send(err);
      });
}