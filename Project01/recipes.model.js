import mongoose from 'mongoose';
let Schema = mongoose.Schema;

var recipeSchema = new Schema({
	name: {type: String, required: true},
	description: {type: String, required: true},
	pictureURL: String,
	prepTime: Number,
	cookTime: {type: Number, required: true},

	//array of direction Strings
	directions: {type: [String]},

	//array of ingredientSchema subdocuments
	ingredients: {type: [ingredientSchema]}, //check this if you have problems** look up subdocument/schema nesting

	//array
	userReview: [{
		//of objectId's
		type: Schema.Types.ObjectID,
		//referencing reviews collection
		ref: 'Reviews'
	}]
});

//ingredient subdocument schema
var ingredientSchema = Schema({
	//always must have an ingredient number
	ingredientName: {type: String, required: true},
	//which ingredient is it? Optional
	ingredientNumber: {type: Number, required: false}
})


let Recipe = mongoose.model('Recipe', recipeSchema);

export{Recipe};