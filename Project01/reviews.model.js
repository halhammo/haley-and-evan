import mongoose from 'mongoose';
let Schema = mongoose.Schema;

var reviewsSchema = new Schema({
	description: {type: String, required: true},
	rating: {type: Number, required: true},
	//server sets date to today
	date: {
		type: Date,
		default: Date.now,
		required: true
	},
	user: {
		type: Schema.Types.ObjectID,
		ref: 'User',
		//required: true
	}
});

//creates review documents
let Reviews = mongoose.model('Reviews', reviewsSchema);
//check on how to export referenced docs

export{Reviews};