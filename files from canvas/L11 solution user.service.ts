import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Users} from '../interfaces/Users';

let url = `/api/users/${userId}`;

@Injectable()
export class UserService {
  static parameters = [HttpClient];
  constructor(private httpClient: HttpClient) {
    this.httpClient = httpClient;
  }
  getAllUsers(): Promise<Users> {
    return this.httpClient
      .get<Users>('/api/users/')
      .toPromise();
  }
  getUserById(userId): Promise<Users> {
    return this.httpClient
        .get<Users>(`/api/users/${userId}`)
        .toPromise();
  }
}
