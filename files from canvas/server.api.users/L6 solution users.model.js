import uuidv4 from 'uuid/v4';

class User {
  users = [];

  find() {
    // Returns a list of all users
    return this.users;
  }

  findById(userId) {
    // Find user by Id
    // Returns user, or null if not present
    let foundUsers = this.users
      .filter(user => user.id === userId);

    if(foundUsers.length > 0) {
      return foundUsers[0];
    } else {
      return null;
    }
  }

  create(user) {
    // Create a new user
    // Return created user
    // Generate the id and overwrite any id that may be present in user
    let id = uuidv4();
    user.id = id;
    this.users.push(user);
    return user;
  }

  findOneAndUpdate(user) {
    // Find user and update
    // If user does not exist, create it using Id provided
    // Return true if user was updated, false if user was created
    let index = this._findUserIndex(user.id);
    if(index >= 0) {
      this.users[index] = user;
      return true;
    } else {
      this.users.push(user);
      return false;
    }
  }

  remove(userId) {
    // Remove user if exists with the Id provided
    // Return true if removed
    // Return false if did user not exist
    let index = this._findUserIndex(userId);
    if(index >= 0) {
      this.users.splice(index, 1);
      return true;
    } else {
      return false;
    }
  }

  _findUserIndex(id) {
    return this.users
      .map(user => user.id)
      .indexOf(id);
  }
}

export default new User();
