import fs from 'fs';
import zlib from 'zlib';

class lab03 {

  syncFileRead(fileName) {
    return fs.readFileSync(fileName).toString();
  }

  asyncFileRead(filename, callback) {
    fs.readFile(filename, (err, data) => {
      if(err) {
        console.error(err);
      } else {
        return callback(data.toString());
      }
    });
  }

  compressFileStream(inputFile, outputFile) {
    let writeStream = fs.createWriteStream(outputFile);
    fs.createReadStream(inputFile)
      .pipe(zlib.createGzip())
      .pipe(writeStream);
    return writeStream;
  }

  decompressFileStream(inputFile, outputFile) {
    let writeStream = fs.createWriteStream(outputFile);
    fs.createReadStream(inputFile)
      .pipe(zlib.createGunzip())
      .pipe(writeStream);
    return writeStream;
  }

  listDirectoryContents(directoryLocation, callback) {
    fs.readdir(directoryLocation, (err, files) => {
      if(err) {
        return console.error(err);
      }
      return callback(files);
    });
  }

}

export {lab03};
