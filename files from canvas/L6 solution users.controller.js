'use strict';

import User from './users.model';

// The export keyword makes the function importable in other files
// (such as /server/api/controller/index.js)
export function index(req, res) {
  // res.json will return the variable as its JSON string representation
  // https://expressjs.com/en/api.html#res.json
  res.json({
    users: User.find()
  });
}

export function show(req, res) {
  // the :id in '/:id' declared by /server/api/controller/index.js can be accessed in the request object
  // under the params object, i.e. req.params.id
  let existingUser = User.findById(req.params.id);

  if(existingUser) {
    // Make sure to only call res.status and res.json *once* per request.
    // If you try to set the status code twice, express.js will give you an error!
    // A detailed explanation for this can be found here:
    // https://stackoverflow.com/questions/7042340/error-cant-set-headers-after-they-are-sent-to-the-client
    res.status(200);
    res.json(existingUser);
  } else {
    // If you don't find the user make sure to return a 404 status code
    // with a descriptive response
    res.status(404);
    res.json({message: 'Not Found'});
  }
}

export function create(req, res) {
  // The JSON you POST when calling /api/controller
  // is parsed automatically into req.body and can be accessed directly
  let name = req.body.name;
  // Validate parameter exists and is a string
  if(!name || typeof name !== 'string') {
    res.status(400);
    return res.json({
      error: 'name(String) is required'
    });
  }

  let address = req.body.address;
  // Validate parameter exists and is a string
  if(!address || typeof address !== 'string') {
    res.status(400);
    return res.json({
      error: 'address(String) is required'
    });
  }

  let age = req.body.age;
  // Validate parameter exists and is a number
  if(!age || typeof age !== 'number') {
    res.status(400);
    return res.json({
      error: 'age(Number) is required'
    });
  }

  // Create a new user object with the generated ID and the fields provided by the user
  let user = {
    name,
    address,
    age
  };

  // Set a status code of 201 (created) and return the new user object back to the caller
  // You need to return the new user so that they can see the generated ID
  res.status(201);
  res.json(User.create(user));
}

export function upsert(req, res) {
  // Create a user object to insert or update
  // Use URL param for ID, Body for name/address/age
  let user = {
    id: req.params.id,
    name: req.body.name,
    address: req.body.address,
    age: req.body.age
  };

  if(User.findOneAndUpdate(user)) {
    res.status(200);
  } else {
    // If user does not exist, set CREATED status code
    res.status(201);
  }
  res.json(user);
}

export function destroy(req, res) {
  if(User.remove(req.params.id)) {
    // If user exists, return NO CONTENT
    res.status(204).send();
  } else {
    // If user does not exist, return NOT FOUND
    res.status(404);
    res.json({message: 'Not Found'});
  }
}
