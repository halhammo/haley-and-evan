//formatting taken from client/app/main/main.components.ts


import {Component, OnInit} from '@angular/core';



@Component({
  selector: 'usermodal',
  template: require('./usermodal.html'),
  styles: [require('./usermodal.scss')],
})
export class UserModalComponent implements OnInit {
  
  //taken from lab13 step 5.2.1.1
  private user: User = {
  __v: undefined,
  _id: undefined,
  address: {
    _id: undefined,
    addressLine1: undefined,
    addressLine2: undefined,
    city: undefined,
    state: undefined,
    zip: undefined,
    __v: undefined
  },
  age: undefined,
  name: {
    _id: undefined,
    firstName: undefined,
    middleName: undefined,
    lastName: undefined
  }
};



  private values: string[];
  private valueToSquare: number;
  private users: User[];
  private input: string;
  static parameters = [HttpClient, UserService, BsModalService];

  constructor(private http: HttpClient, private userService: UserService, private modalService: BsModalService) {
    this.http = http;
    this.userService = userService;
    this.modalService = modalService;
    this.setData();
    this.getUserData();

    setTheme('bs3'); //or 'bs4'

  }

  private setData() {
    this.values = ['first', 'second', 'third'];
    this.valueToSquare = 4;
  }

  public getUserData() {
    this.userService.getAllUsers()
      .then(response => {
        this.users = response.users as User[];
      })
      .catch(this.handleError);
  }

  public editUser(user: User) {
    const initialState = {
      user
    }
    const modalRef = this.modalService.show(UpdateUserComponent, {initialState});
    modalRef.content.updatedUser.subscribe(() => {
      this.userService.updateUser(user)
        .then(updatedUser => {
          modalRef.content.formInfo = `User ${updatedUser._id} updated!`;
        })
        .catch(err => {
          console.log(err);
          modalRef.content.formError = err.error.message;
        });
    });
  }

  private handleError(error: any): Promise<any> {
    console.error('Something has gone wrong', error);
    return Promise.reject(error.message || error);
  }

  ngOnInit() {
  }
}
