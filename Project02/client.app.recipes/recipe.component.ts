import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import { BsModalService } from 'ngx-bootstrap';
import { HttpClient } from '@angular/common/http';

import {Review} from '../../components/interfaces/Review';
// import {ReviewService} from '../../components/services/review.service';

import {Recipe} from '../../components/interfaces/Recipe';
import {RecipeService} from '../../components/services/recipe.service';
import {CreateRecipeComponent} from "../../components/modals/create-recipe.component";
import {UpdateRecipeComponent} from "../../components/modals/update-recipe.component";

import {CreateReviewComponent} from "../../components/modals/create-review.component";
import {UpdateReviewComponent} from "../../components/modals/update-review.component";


@Component({
  selector: 'recipe',
  template: require('./recipe.html'),
  styles: [require('./recipe.scss')],
})
export class RecipeComponent implements OnInit {
  
  private recipe: Recipe;
  private recipes: Recipe[];

  private review: Review;
  private reviews: Review[];

  static parameters = [HttpClient, ActivatedRoute, BsModalService, RecipeService];

  constructor(private http: HttpClient, private recipeService: RecipeService, private route: ActivatedRoute, private modalService: BsModalService) {
    this.http = http;
    this.route = route;
    this.modalService = modalService;
    this.recipeService = recipeService;
  }


  updateRecipe(recipe: Recipe)
  {
    const initialState = {
      recipe
    }
    const modalRef = this.modalService.show(UpdateRecipeComponent, {initialState});
    modalRef.content.updatedRecipe.subscribe(() => {
      this.recipeService.updateRecipe(recipe)
        .then(updatedRecipe => {
          modalRef.content.formInfo = `Recipe ${updatedRecipe._id} updated!`;
        })
        .catch(err => {
          console.log(err);
          modalRef.content.formError = err.error.message;
        });
    });
  }


  deleteRecipe(recipe: Recipe) {
    this.recipeService.deleteRecipe(recipe._id)
      .then(response => {
        alert('Successful Delete');
        console.log('succ');
      })
      .catch(error => {
        console.error(error);
      });
  }

  /*

  createReview(recipe) {
    const modalRef = this.modalService.show(CreateReviewComponent);
    modalRef.content.reviewToCreate.subscribe(() => {
      this.recipeService.createReview(recipe)
        .then(createdReview => {
          modalRef.content.formInfo = `Recipe ${createdReview._id} created!`;
        })
        .catch(err => {
          console.log(err);
          modalRef.content.formError = err.error.message;
        });
    });
  }


  updateReview(recipe, review) {
     const modalRef = this.modalService.show(UpdateReviewComponent);
     modalRef.content.reviewToUpdate.subscribe(() => {
       this.recipeService.updateReview(recipe, review)
         .then(updatedReview => {
           modalRef.content.formInfo = `Review ${updatedReview._id} updated!`;
         })
         .catch(err => {
           console.log(err);
           modalRef.content.formError = err.error.message;
         });
     });
  }


  deleteReview(recipe, review) {
    this.recipeService.deleteReview(recipe._id, review._id)
      .then(response => {
        alert('Successful Delete');
        console.log('succ');
      })
      .catch(error => {
        console.error(error);
      });
  }

  */


  ngOnInit() {
    this.route.params.subscribe(params => {
      this.recipeService.getRecipeById(params.id)
        .then(recipe => {
          this.recipe = recipe;
        });
    });
  }
}
