import {Review} from "./Review";

export interface Reviews {
    reviews: Review[];
}
