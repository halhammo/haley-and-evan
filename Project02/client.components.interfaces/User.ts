export interface User {
    _id: string;
    fullname: {
        _id: string,
        firstName: string,
        lastName: string
    };
    username: string;
    email: string;
    __v: number;
}