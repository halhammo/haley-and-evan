import {Recipe} from "./Recipe";

export interface Recipes {
    recipes: Recipe[];
}
