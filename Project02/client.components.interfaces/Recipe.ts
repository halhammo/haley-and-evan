export interface Ingredients {
    name: string;
    amount: string;
}

export interface Recipe {
    _id: string;
    name: string;
    description: string;
    image: string;
    prep_time: number;
    cooking_time: number;
    directions: [string]; // i think this is the right syntax?
    ingredients: [Ingredients];
    //reviews: object;
    __v: number;
}