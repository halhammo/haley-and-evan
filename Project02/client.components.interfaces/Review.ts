import {User} from './User';
import { DatePipe } from '@angular/common';

export interface Review {
    _id: string;
    description: string;
    rating: number;
    date: number;
    user: User;
}