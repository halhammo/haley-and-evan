import { Component, EventEmitter, Input, Output, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';

import { BsModalRef } from 'ngx-bootstrap/modal';
import { User } from '../interfaces/User';
import {UserService} from '../services/user.service';


@Component({
  selector: 'update-user',
  template: require('./update-user.html')
})
export class UpdateUserComponent {
  @Input()
  user: User;

  @Input()
  formError: String;

  @Input()
  formInfo: String;

  @Output()
  updatedUser: EventEmitter<User> = new EventEmitter<User>();

  static parameters = [BsModalRef];
  constructor(public bsModalRef: BsModalRef) {}

  updateUser() {
    this.updatedUser.emit(this.user);
  }
}
