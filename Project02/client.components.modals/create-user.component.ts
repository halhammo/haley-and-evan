import { Component, EventEmitter, Input, Output, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';

import { BsModalRef } from 'ngx-bootstrap/modal';
import { User } from '../interfaces/User';
import {UserService} from '../services/user.service';

@Component({
  selector: 'create-user',
  template: require('./create-user.html')
})
export class CreateUserComponent {
  @Input()
  formError: String;

  @Input()
  formInfo: String;

  @Output()
  userToCreate: EventEmitter<User> = new EventEmitter<User>();

  private user: User = {
    __v: undefined,
    _id: undefined,
    fullname: {
      _id: undefined,
      firstName: undefined,
      lastName: undefined
    },
    username: undefined,
    email: undefined
  };

  static parameters = [BsModalRef];
  constructor(public bsModalRef: BsModalRef) {}

  createUser() {
    this.userToCreate.emit(this.user);
  }

}
