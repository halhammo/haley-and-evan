import { Component, EventEmitter, Input, Output, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';

import { BsModalRef } from 'ngx-bootstrap/modal';
import { Recipe } from '../interfaces/Recipe';
import {Ingredients} from '../interfaces/Recipe';
//import {Review} from '../interfaces/Review';
import {RecipeService} from '../services/recipe.service';

@Component({
    selector: 'create-recipe',
    template: require('./create-recipe.html'),
    providers: [RecipeService]
})
export class CreateRecipeComponent implements OnInit{
    @Input()
    formError: String;

    @Input()
    formInfo: String;

    @Output()
    recipeToCreate: EventEmitter<Recipe> = new EventEmitter<Recipe>();

    private ingredients: Ingredients = {
        name: undefined,
        amount: undefined,
    };

    //private reviews: Review = {
      //  description: undefined,
      //  rating: undefined,
      //  date:undefined, // how do you do date.now?
      //  user: undefined,
    //};

    private recipe: Recipe = {
        _id: undefined,
        name: undefined,
        description: undefined,
        image: undefined,
        prep_time: undefined,
        cooking_time: undefined,
        directions: [undefined], // i think this is the right syntax?
        ingredients: [this.ingredients],
        //reviews: [this.reviews],
        __v: 0,
    };

    static parameters = [BsModalRef, ActivatedRoute, RecipeService];
    constructor(public bsModalRef: BsModalRef, private route: ActivatedRoute, private recipeService: RecipeService) {
        this.route = route;
        this.recipeService = recipeService;
    }

    createRecipe() {
        this.recipeToCreate.emit(this.recipe);
    }

    private handleError(error: any): Promise<any> {
        console.error('Something went wrong! : ', error);
        return Promise.reject(error.message || error);
    }

    ngOnInit() {
        this.route.params.subscribe(params =>  {
            this.recipeService.getRecipeById(params.id)
                .then(recipe => {
                    this.recipe = recipe;
                })
                .catch(this.handleError);
        });
    }
}