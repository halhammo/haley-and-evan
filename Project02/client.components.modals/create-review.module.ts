import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule, ReactiveFormsModule, FormBuilder, FormGroup, FormArray, FormControl} from '@angular/forms';
import {ModalModule} from 'ngx-bootstrap';

import {CreateReviewComponent} from './create-review.component';

import { RatingModule } from 'ngx-bootstrap/rating';


@NgModule({
    imports: [
        ModalModule.forRoot(),
        BrowserModule,
        FormsModule,
        ReactiveFormsModule,
        FormBuilder,
        FormGroup,
        FormArray,
        FormControl,
        RatingModule.forRoot()
    ],
    declarations: [
        CreateReviewComponent,
    ],
    exports: [
        CreateReviewComponent,
    ],
    providers: [],
    entryComponents: [
        CreateReviewComponent,
    ]

})
export class CreateReviewModule {}