import { Component, EventEmitter, Input, Output, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { Review } from '../interfaces/Review';
import {RecipeService} from '../services/recipe.service';
import {User} from '../interfaces/User';
import {UserService} from '../services/user.service';

//import { DatePipe } from '@angular/common';



@Component({
  selector: 'create-review',
  template: require('./create-review.html')
})
export class CreateReviewComponent implements OnInit{
  @Input()
  formError: String;

  @Input()
  formInfo: String;

  @Output()
  reviewToCreate: EventEmitter<Review> = new EventEmitter<Review>();

  private user: User = {
    __v: undefined,
    _id: undefined,
    fullname: {
      _id: undefined,
      firstName: undefined,
      lastName: undefined,
    },
  username: undefined,
  email: undefined,
  };

  private review: Review = {
    _id: undefined,
    description: undefined,
    rating: undefined,
    date: number,
    //date: Date = Date.now(), //I think this sets date automatically for you?
    user: this.user //not sure if this is correct
  };

  static parameters = [BsModalRef, ActivatedRoute];
  constructor(public bsModalRef: BsModalRef, private route: ActivatedRoute, private recipeService: RecipeService) {
    this.route = route;
    this.recipeService = recipeService;
  }

  createReview() {
    this.reviewToCreate.emit(this.review);
    // this.reviewService = reviewService;
  }

  // private handleError(error: any): Promise<any> {
  //   console.error('Something went wrong! : ', error);
  //   return Promise.reject(error.message || error);
  // }

  ngOnInit() {
    // this.route.params.subscribe(params =>  {
    //   this.recipeService.getReviewById(params.id)
    //       .then(review => {
    //         this.review = review;
    //       })
    //       .catch(this.handleError);
    // });
  }

}
