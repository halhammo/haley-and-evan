import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';


import { RouterModule, Routes } from '@angular/router';

import { TooltipModule } from 'ngx-bootstrap';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MainComponent } from './main.component';
import {SquarePipe} from '../../components/pipes/square.pipe';

//import user CRUD
import {UserService} from '../../components/services/user.service';
import {UpdateUserModule} from '../../components/modals/update-user.module';
import {CreateUserModule} from '../../components/modals/create-user.module';


//import recipe CRUD
import {RecipeService} from '../../components/services/recipe.service';
import {UpdateRecipeModule} from '../../components/modals/update-recipe.module';
import {CreateRecipeModule} from '../../components/modals/create-recipe.module';



//4.1.1 ngx-bootstrap Modal Dialog
import {ModalModule, BsModalRef} from 'ngx-bootstrap/modal';

export const ROUTES: Routes = [
    { path: 'home', component: MainComponent },
];

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        BrowserAnimationsModule,

        //user CRUD
        UpdateUserModule,
        CreateUserModule,

        //recipes CRUD
        UpdateRecipeModule,
        CreateRecipeModule,

        
        RouterModule.forChild(ROUTES),

        TooltipModule.forRoot(),

        //4.1.1 ngx-bootstrap Modal Dialog
        ModalModule.forRoot()
    ],
    declarations: [
        MainComponent,
        SquarePipe
    ],

    exports: [
        MainComponent,
    ],

    providers: [
      UserService,
    ]
})
export class MainModule {}
