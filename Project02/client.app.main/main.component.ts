import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';


import {UserService} from '../../components/services/user.service';
import {User} from '../../components/interfaces/User';
//user CRUD
import {CreateUserComponent} from '../../components/modals/create-user.component';
import {UpdateUserComponent} from '../../components/modals/update-user.component';


import {RecipeService} from "../../components/services/recipe.service";
import {Recipe} from "../../components/interfaces/Recipe";
//recipe CRUD
import {CreateRecipeComponent} from '../../components/modals/create-recipe.component';
import {UpdateRecipeComponent} from '../../components/modals/update-recipe.component';


import {Reviews} from "../../components/interfaces/Reviews";
//review CRUD
import {CreateReviewComponent} from '../../components/modals/create-review.component';
import {UpdateReviewComponent} from '../../components/modals/update-review.component';


import { BsModalService } from 'ngx-bootstrap';

@Component({
  selector: 'main',
  template: require('./main.html'),
  styles: [require('./main.scss')],
  providers: [UserService, RecipeService]
})
export class MainComponent implements OnInit {

  private values: string[];
  private valueToSquare: number;
  private users: User[];
  private recipes: Recipe[];
  private input: string;
  static parameters = [HttpClient, UserService, RecipeService, BsModalService];

  constructor(private http: HttpClient, private userService: UserService, private recipeService: RecipeService, private modalService: BsModalService) {
    this.http = http;
    this.userService = userService;
    this.recipeService = recipeService;
    this.modalService = modalService;
    this.getUserData();
    this.getRecipeData();
  }

  public getUserData() {
    this.userService.getAllUsers()
      .then(response => {
        this.users = response.users as User[];
      })
      .catch(this.handleError);
  }

  public getRecipeData() {
    this.recipeService.getAllRecipes()
        .then(response => {
          this.recipes = response.recipes as Recipe[];
        })
        .catch(this.handleError);
  }

  createUser() {
    const modalRef = this.modalService.show(CreateUserComponent);
    modalRef.content.userToCreate.subscribe(userToCreate => {
      this.userService.createUser(userToCreate)
          .then(createdUser => {
            modalRef.content.formInfo = `User ${createdUser._id} created.`;
          })
          .catch(err => {
            console.log(err);
            modalRef.content.formError = err.error.message;
          });
    });
  }


  updateUser() {
      const modalRef = this.modalService.show(UpdateUserComponent);
      modalRef.content.userToUpdate.subscribe(userToUpdate => {
        this.userService.updateUser(userToUpdate)
            .then(updatedUser => {
              modalRef.content.formInfo = `User ${updatedUser._id} updated.`;
            })
            .catch(err => {
              console.log(err);
              modalRef.content.formError = err.error.message;
            });
      });
    }

  createRecipe() {
    const modalRef = this.modalService.show(CreateRecipeComponent);
    modalRef.content.recipeToCreate.subscribe(recipeToCreate => {
      this.recipeService.createRecipe(recipeToCreate)
          .then(createdRecipe => {
            modalRef.content.formInfo = `Recipe ${createdRecipe._id} created.`;
          })
          .catch(err => {
            console.log(err);
            modalRef.content.formError = err.error.message;
          });
    });
  }



    createReview() {
    const modalRef = this.modalService.show(CreateReviewComponent);
    modalRef.content.reviewToCreate.subscribe(reviewToCreate => {
      this.recipeService.createReview(reviewToCreate)
          .then(createdReview => {
            modalRef.content.formInfo = `User ${createdReview._id} created.`;
          })
          .catch(err => {
            console.log(err);
            modalRef.content.formError = err.error.message;
          });
    });
  }

  

  private handleError(error: any): Promise<any> {
    console.error('Something has gone wrong', error);
    return Promise.reject(error.message || error);
  }

  ngOnInit() {
  }
}
