import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Recipes} from '../interfaces/Recipes';
import {Recipe} from '../interfaces/Recipe';
import {Reviews} from '../interfaces/Reviews';
import {Review} from '../interfaces/Review';


@Injectable()
export class RecipeService {
    static parameters = [HttpClient];
    constructor(private httpClient: HttpClient) {
        this.httpClient = httpClient;
    }

    createRecipe(recipe: Recipe): Promise<Recipe> {
    return this.httpClient
      .post<Recipe>(`/api/recipes/`, recipe)
      .toPromise();
    }

    getAllRecipes(): Promise<Recipes> {
        return this.httpClient
            .get<Recipes>('/api/recipes/')
            .toPromise();
    }
    getRecipeById(recipe: Recipe): Promise<Recipe> {
        return this.httpClient
            .get<Recipe>(`/api/recipes/${recipe._id}`)
            .toPromise();
    }

    updateRecipe(recipe: Recipe): Promise<Recipe> {
    return this.httpClient
      .put<Recipe>(`/api/recipes/${recipe._id}`, recipe)
      .toPromise();
    }

    deleteRecipe(recipeId): Promise<Recipe>{
        return this.httpClient
          .delete<Recipe>(`/api/recipes/${recipeId}`)
          .toPromise();
    }

    //reviews are handled with recipes because they're really good friends

    createReview(recipe: Recipe): Promise<Review>{
    return this.httpClient
        .post<Review>(`/api/recipes/${recipe._id}/reviews`, recipe)
        .toPromise()
    }

     getAllReviews(): Promise<Reviews> {
        return this.httpClient
            .get<Reviews>('/api/reviews/')
            .toPromise();
    }

    // getReviewById(recipe: Recipe, review: Review): Promise<Review> {
    //     return this.httpClient
    //         .get<Review>(`/api/recipes/${recipe._id}/reviews/${review._id}`, recipe)
    //         .toPromise();
    // }

    updateReview(recipe: Recipe, review: Review): Promise<Review>{
        return this.httpClient
            .put<Review>(`/api/recipes/${recipe._id}/review/${review._id}`, recipe)
            .toPromise();
    }

    deleteReview(recipeId, reviewId): Promise<Review>{
        return this.httpClient
            .delete<Review>(`/api/recipes/${recipeId}/reviews/${reviewId}`)
            .toPromise();
    }


}